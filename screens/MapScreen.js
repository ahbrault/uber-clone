import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import tw from "tailwind-react-native-classnames";
import Map from "../components/Map";
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import NavigateCard from "../components/NavigateCard";
import RideOptionsCard from "../components/RideOptionsCard";

const MapScreen = () => {
    const Stack = createNativeStackNavigator();
    const options = {
        headerShown: false
    };

    return (
        <View>
            <View style={tw`h-1/2`}>
                <Map/>
            </View>
            <View style={tw`h-1/2`}>
                <Stack.Navigator>
                    <Stack.Screen name={"NavigateCard"} component={NavigateCard} options={options}/>
                    <Stack.Screen name={"RideOptionsCard"} component={RideOptionsCard} options={options}/>
                </Stack.Navigator>
            </View>
        </View>
    );
};

export default MapScreen;

const styles = StyleSheet.create({});