import React from 'react';
import {Text, View, StyleSheet, SafeAreaView} from 'react-native';
import tw from "tailwind-react-native-classnames";

const RideOptionsCard = () => {
    return (
        <SafeAreaView style={tw`bg-white flex-1`}>
            <Text>Pick a ride</Text>
        </SafeAreaView>
    );
};

export default RideOptionsCard;

const styles = StyleSheet.create({});