import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {Provider} from 'react-redux';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import HomeScreen from "./screens/HomeScreen";
import {store} from "./store";
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import MapScreen from "./screens/MapScreen";

const App = () => {
    const Stack = createNativeStackNavigator();
    const options = {
        headerShown: false
    };

    return (
        <Provider store={store}>
            <NavigationContainer>
                <SafeAreaProvider>
                    <Stack.Navigator>
                        <Stack.Screen name={"HomeScreen"} component={HomeScreen} options={options}/>
                        <Stack.Screen name={"MapScreen"} component={MapScreen} options={options}/>
                    </Stack.Navigator>
                </SafeAreaProvider>
            </NavigationContainer>
        </Provider>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default App;